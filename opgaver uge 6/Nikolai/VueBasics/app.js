const app = Vue.createApp({
    data(){
        return{
            url:'https://www.raahauge.eu',
            showBooks: true,
            books:[
                {title:'Digital fortress', author:'Dan Brown',img:'assets/1.jpg', isFav:true},
                {title: 'Da vinci code', author:'Dan Brown',img:'assets/2.jpg', isFav:false},
                {title: 'Origin', author:'Dan Brown',img:'assets/3.jpg', isFav:true},
            ],
            x: 0,
            y: 0
        }
    },
    methods:{
        ChangeTitle(title){
            this.title = title
        },
        ToggleBooks(){
            this.showBooks = !this.showBooks
        },
        handleEvent(e, data){
            console.log(e.type+" Event triggered")
            if (data){
                console.log(data)
            }
        },
        handleMouseMove(e){
            this.x = e.offsetX
            this.y = e.offsetY
            //console.log(this.x,this.y)
        },
        toggleFav(book){
            book.isFav = !book.isFav
        }
    },
    computed: {
     FilteredBooks(){
         return this.books.filter((book)=> book.isFav)
     }   
    }
    //template: '<h2>I am the template now</h2> <p>{{ title }}</p>',
})

app.mount('#app')