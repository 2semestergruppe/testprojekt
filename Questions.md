# Spørgsmål som vi har brug for svar på

1)  - Er der brug for diagrammer ift. Læringsmål? **BESVARET**\
    ***Svar*** - Hvis det giver mening så kan vi gøre brug af det, alt efter hvad der er af behov
2)  - Er der brug for brugertests ift. Læringsmål? **BESVARET**\
    ***Svar*** - Nej er der ikke brug for, og der kan være placeholder tekst på det meste.
3) - Frontend eco system **BESVARET**
    - Vi mangler noget afklaring på hvad det omfatter?\
    ***Svar*** - Alle de ting der er omkring vue er økosystem. værktøjer, npm, cli, dokumentation. 
        https://medium.com/js-dojo/vue-ecosystem-979773a9bf54
        For at få det opfyldt så skal vi huske at dokumenterer hvorfor og hvordan vi har gjort brug af Vue og dets økosystem.
4) - Frontend workflows **BESVARET**
    - Vi mangler noget afklaring på hvad det omfatter, vi tænker det er arbejdsgangen for os og hvordan vi kommer i mål med vores tasks, men er det rigtigt forstået?\
    ***Svar*** - Deployment, hvordan får vi det fra development til launch. 
    hvordan får vi lavet css mapper eller opbygger vores projekt, hvor starter vi og hvad kommer vi igennem under udviklingen af projektet.
5) - Vælge egnede værktøjer og metoder til implementering af client-side web applikationer
    - Vi bruger Code til at kode i. Vue som framework, men er der andet der skal med som værktøj? **BESVARET**\
    ***Svar*** - Git, Gitlab, Node.Js, Sass loaderen, v8 enginen i JS, Test hvis vi bruger appen i flere browsere, debugging hvis vi gør brug af det, Postman. Bør blive opdateret løbende som vi laver projektet, hvis der skulle komme flere værktøjer frem. 
6) - Anvende metoder til kvalitetssikring af client-side web applikationer
    - Burde blive opfyldt som vi kommer igennem projektet, så længe Scrum tæller for en metode, men gør det, det? **BESVARET**\
    ***Svar*** - Vi kommer indover testing med Morten på et senere tidspunkt i undervisningen. Og så længe dokumentationen er god, så hvis der var nogen der skulle overtage vores projekt, og de kunne gå direkte ind i projektet uden vores indblanding så har vi gjort det som vi skal. Gør brug af Bim og kommentarer i sin kode. eslint.

## Foregående spørgsmål blev besvaret d. 03/03

    
